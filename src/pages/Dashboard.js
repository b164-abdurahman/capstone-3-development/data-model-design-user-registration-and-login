import React from 'react';

import DashboardSection1 from '../components/DashboardSection1';
import DashboardSection2 from '../components/DashboardSection2';

const Dashboard = () => {
  return (
    <>
      <DashboardSection1 />
      <DashboardSection2 />
    </>
  );
};

export default Dashboard;
